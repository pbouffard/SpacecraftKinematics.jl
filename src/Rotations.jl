module Rotations
using ..Utils

using LinearAlgebra
# using Infiltrator

export tilde
export normsq

export C1, C1d, C2, C2d, C3, C3d, C321⁻¹
export C321, C321d, C123, C123d

export DCM2PRV, PRV2DCM, pointingDCM

export DCM2quat, quat2DCM
export quat_wxyz2xyzw, quat_xyzw2wxyz
export q321dxyzw, q123dxyzw, q123dwxyz, q321dwxyz

export DCM2MRP, DCM2MRPv2, MRP2DCM, compose_mrp, quat2MRP, mrp_shadow, MRP_ω_from_σ_and_σ̇, MRP_Bmatrix_derivative, MRP_ω̇_from_σ_and_derivs, MRP_Bmatrix, dσdt_mrp

# TODO: Use StaticArrays and custom types

ensure_rotation_matrix(C; name="input") = ensure_size(C, (3,3)) && ensure_orthonormal(C, name=name) && det(C) ≈ 1 || error("$name is not a rotation matrix (determinant = $(det(C)))")

ensure_quaternion(β) = ensure_length(β, 4, name="input vector") && ensure_unitvector(β, name="input vector")

"""
cross-product matrix
"""
function tilde(x)
  ensure_length(x, 3)
  # z = zero(x[1]) # trick to make this play nice with Unitful.jl
  z = zero(x)[1]
  [    
     z -x[3] x[2]; 
   x[3]   z -x[1]; 
  -x[2] x[1]   z
  ]
end

normsq(v) = sum(map(x -> x^2, v))

# MARK: Euler Angles

# Body angular velocity vector as function of 3-2-1 Euler angles and their rates
ᴮω321(ψ, θ, ϕ, ψ̇, θ̇, ϕ̇) = [
        -sin(θ) 0 1; 
        sin(ϕ)*cos(θ) cos(ϕ) 0;
        cos(ϕ)*cos(θ) -sin(ϕ) 0
    ] * [ψ̇ θ̇ ϕ̇]'
const body_ω321 = ᴮω321 # Easier to tab-complete

# Time derivative of 3-2-1 Euler angles given body angular velocity vector in body frame ᴮω
B321(ψ, θ, ϕ) = 1/cos(θ) * [
         0          sin(ϕ)        cos(ϕ); 
         0      cos(ϕ)*cos(θ)  -sin(ϕ)*cos(θ); 
        cos(θ)  sin(ϕ)*sin(θ)   cos(ϕ)*sin(θ)
    ]
d321dt(ψ, θ, ϕ, ᴮω) = B321(ψ, θ, ϕ) * ᴮω

# Body angular velocity vector as function of 3-1-3 Euler angles and their rates
ᴮω313(θ₁, θ₂, θ₃, θ̇₁, θ̇₂, θ̇₃) = [
    sin(θ₃)*sin(θ₂) cos(θ₃) 0;
    cos(θ₃)*sin(θ₂) -sin(θ₃) 0;
    cos(θ₂) 0 1
] * [θ̇₁ θ̇₂ θ̇₃]'
const body_ω313 = ᴮω313 # Easier to tab-complete

# Time derivative of 3-2-1 Euler angles given body angular velocity vector in body frame ᴮω
B313(θ₁, θ₂, θ₃) = 1/sin(θ₂) * [
    sin(θ₃) cos(θ₃) 0;
    cos(θ₃)*sin(θ₂) -sin(θ₃)*sin(θ₂) 0;
    -sin(θ₃)*cos(θ₂) -cos(θ₃)*cos(θ₂) sin(θ₂)
]
d313dt(θ₁, θ₂, θ₃, ᴮω) = ensure_length(ᴮω, 3; name="angular velocity vector") && B313(θ₁, θ₂, θ₃) * ᴮω

# MARK: DCM - Direction Cosine Matrix
C1(ϕ) = [1 0 0; 0 cos(ϕ) sin(ϕ); 0 -sin(ϕ) cos(ϕ)]
C1d(ϕ) = [1 0 0; 0 cosd(ϕ) sind(ϕ); 0 -sind(ϕ) cosd(ϕ)]

C2(θ) = [cos(θ) 0 -sin(θ); 0 1 0; sin(θ) 0 cos(θ)]
C2d(θ) = [cosd(θ) 0 -sind(θ); 0 1 0; sind(θ) 0 cosd(θ)]

C3(ψ) = [cos(ψ) sin(ψ) 0; -sin(ψ) cos(ψ) 0; 0 0 1]
C3d(ψ) = [cosd(ψ) sind(ψ) 0; -sind(ψ) cosd(ψ) 0; 0 0 1]

C123(ϕ, θ, ψ) = C3(ψ) * C2(θ) * C1(ϕ)
C123d(ϕ, θ, ψ) = C3d(ψ) * C2d(θ) * C1d(ϕ)

C321(ψ, θ, ϕ) = C1(ϕ) * C2(θ) * C3(ψ)
C321d(ψ, θ, ϕ) = C1d(ϕ) * C2d(θ) * C3d(ψ)
C321⁻¹(C) = ensure_rotation_matrix(C) && (ψ = atan(C[1,2], C[1,1]), θ = -asin(C[1,3]), ϕ = atan(C[2,3], C[3,3]))

C313(θ₁, θ₂, θ₃) = C3(θ₃) * C1(θ₂) * C3(θ₁)
C313⁻¹(C) = (θ₁ = ensure_rotation_matrix(C) && atan(C[3,1], -C[3,2]), θ₂ = acos(C[3,3]), θ₃ = atan(C[1,3], C[2,3]))
C232(θ₁, θ₂, θ₃) = C2(θ₃) * C3(θ₂) * C2(θ₁)

# MARK: Principal Rotation Vectors (PRV) a.k.a. Axis-Angle
function DCM2PRV(C)
  ensure_rotation_matrix(C)
  cosΦ = 1/2 * (tr(C) - 1)
  # @show cosΦ
  Φ = acos(cosΦ)
  # @show Φ
  ê = abs(Φ) < 1e-5 ? [1; 0; 0] : 1/(2 * sin(Φ)) * [C[2,3] - C[3,2]; C[3,1] - C[1,3]; C[1,2] - C[2,1]]
  (;Φ, ê)
end

function PRV2DCM(Φ, ê)
  sΦ, cΦ = sincos(Φ)
  Σ = 1 - cΦ
  e1, e2, e3 = ê
  C = [
    e1^2*Σ + cΦ       e1*e2*Σ + e3*sΦ   e1*e3*Σ - e2*sΦ
    e1*e2*Σ - e3*sΦ   e2^2*Σ + cΦ       e2*e3*Σ + e1*sΦ
    e1*e3*Σ + e2*sΦ   e2*e3*Σ - e1*sΦ   e3^2*Σ + cΦ
  ]
end

"""
Return DCM that rotates X axis to point towards given direction.
"""
function  pointingDCM(v1)
  u1 = v1[:]/norm(v1) # convert to vector and normalize
  ensure_length(u1, 3)
  ensure_unitvector(u1)
  u2 = orthogonal_vector(u1)
  u3 = cross(u1, u2)
  reshape([u1; u2; u3], (3,3))
end

function orthogonal_vector(v_in)
  v = copy(v_in)/norm(v_in)
  m = 1
  # because v1 is unit vector, some v1[m] ∈ {1,2,3} has to have been nonzero
  # find a nonzero entry
  while v[m] ≈ 0
    m += 1
  end
  n = first(setdiff(1:3, m)) # pick an n ≠ m
  u = zeros(3)
  # swap the m and n entries and negate one; guarantees u ≠ v 
  u[m] = v[n]
  u[n] = -v[m]
  u = u/norm(u)
end

# MARK: Quaternions/Euler Parameters
# (β = [β₀, β₁, β₂, β₃], β₀ = scalar part)

quat_wxyz2xyzw(β) = ensure_length(β, 4) && [β[2], β[3], β[4], β[1]]
quat_xyzw2wxyz(β) = ensure_length(β, 4) && [β[4], β[1], β[2], β[3]]

# EP from PRV
function PRV2quat(Φ, ê)
  ensure_length(ê, 3)
  [cos(Φ/2); ê*sin(Φ/2)]
end

# DCM from quaternion
function quat2DCM(β)
  ensure_quaternion(β)
  [β[1]^2 + β[2]^2 - β[3]^2 - β[4]^2 2(β[2]β[3] + β[1]β[4]) 2(β[2]β[4] - β[1]β[3]);
  2(β[2]β[3] - β[1]β[4]) β[1]^2 - β[2]^2 + β[3]^2 - β[4]^2 2(β[3]β[4] + β[1]β[2]);
  2(β[2]β[4] + β[1]β[3]) 2(β[3]β[4] - β[1]β[2]) β[1]^2 - β[2]^2 - β[3]^2 + β[4]^2]
end

"""
Sheppards' method to determine quaternion from DCM
"""
function DCM2quat(C)
  ensure_rotation_matrix(C)

  C == I && return [1., 0, 0, 0] # special case

  β₀² = 1/4*(1 + tr(C))
  β₁² = 1/4*(1 + 2C[1,1] - tr(C))
  β₂² = 1/4*(1 + 2C[2,2] - tr(C))
  β₃² = 1/4*(1 + 2C[3,3] - tr(C))
  βs = [β₀², β₁², β₂², β₃²]
  
  imax = argmax(βs)

  terms = 1/4 * [
      C[2,3] - C[3,2],
      C[3,1] - C[1,3],
      C[1,2] - C[2,1],
      C[1,2] + C[2,1],
      C[3,1] + C[1,3],
      C[2,3] + C[3,2],
  ]
  
  if imax == 1
      β₀ = sqrt(β₀²)
      β = [β₀, terms[1]/β₀, terms[2]/β₀, terms[3]/β₀]
  elseif imax == 2
      β₁ = sqrt(β₁²)
      β = [terms[1]/β₁, β₁, terms[4]/β₁, terms[5]/β₁]
  elseif imax == 3
      β₂ = sqrt(β₂²)
      β = [terms[2]/β₂, terms[4]/β₂, β₂, terms[6]/β₂]
  elseif imax == 4
      β₃ = sqrt(β₃²)
      β = [terms[3]/β₃, terms[5]/β₃, terms[6]/β₃, β₃]
  else
      error("Invalid imax $(imax)")
  end
  
  return β
end

# Successive rotation of β1 followed by β2
function compose_quats(β1, β2)
  ensure_quaternion(β1)
  ensure_quaternion(β2)
  M = [
      β2[1] -β2[2] -β2[3] -β2[4];
      β2[2]  β2[1]  β2[4] -β2[3];
      β2[3] -β2[4]  β2[1]  β2[2];
      β2[4]  β2[3] -β2[2]  β2[1];
  ]
  return M*β1
end

# Time derivative of quaternion β given body angular velocity vector in body frame ᴮω
dβdt(β, ᴮω) = (
  ensure_quaternion(β) 
  && ensure_length(ᴮω, 3, name="input angular velocity vector") 
  && 1/2 * [
      β[1] -β[2] -β[3] -β[4];
      β[2]  β[1] -β[4]  β[3];
      β[3]  β[4]  β[1] -β[2];
      β[4] -β[3]  β[2]  β[1];
  ] * [0 ᴮω]'
)

# Convenience functions

"Form xyzw quaternion from 3-2-1 rotation in degrees"
q321dxyzw(a3, a2, a1) = quat_wxyz2xyzw(DCM2quat(C321d(a3, a2, a1)))
q321dwxyz(a3, a2, a1) = DCM2quat(C321d(a3, a2, a1))

"Form xyzw quaternion from 1-2-3 rotation in degrees"
q123dxyzw(a3, a2, a1) = quat_wxyz2xyzw(DCM2quat(C123d(a1, a2, a3)))
q123dwxyz(a3, a2, a1) = DCM2quat(C123d(a1, a2, a3))

# MARK: Classical Rodrigues Parameters (CRP) q = [q₁, q₂, q₃]

# DCM from CRP
CRP2DCM(q) = ensure_length(q, 3) && 1/(1 + q'q) * ((1 - q'q)*I + 2q*q' - 2*tilde(q))

# CRP from DCM
DCM1CRP(C) = ensure_rotation_matrix(C) && 1/(tr(C) + 1) * [C[2,3] - C[3,2]; C[3,1] - C[1,3]; C[1,2] - C[2,1]]

# Successive rotation of q1 followed by q2
compose_crp(q1, q2) = ensure_length(q1, 3) && ensure_length(q2, 3) && (q2 + q1 - cross(q2, q1))/(1 - q2'q1)

# Time derivative of CRP q for angular velocity ω
dqdt(q, ω) = ensure_length(q, 3) && ensure_length(ω, 3) && 1/2 * (I  + tilde(q) + q*q') * ω


# MARK: Modified Rodrigues Parameters (MRP) σ = [σ₁, σ₂, σ₃]

"""
Convert MRP to DCM
"""
function MRP2DCM(σ)
  # @info "In MRP2DCM"
  # ensure_length(σ, 3) &&
  
  return I + (8*tilde(σ)^2 - 4(1 - normsq(σ))*tilde(σ))/((1 + normsq(σ))^2)
end

"""
Convert DCM to MRP
"""
function DCM2MRP(C)
  ensure_rotation_matrix(C)
  ζ = sqrt(tr(C) + 1)
  # @show ζ, ζ ≈ 0
  if ζ < 0.01
    return DCM2MRPv2(C)
  else
    1/(ζ*(ζ + 2)) * [C[2,3] - C[3,2]; C[3,1] - C[1,3]; C[1,2] - C[2,1]]
  end
end

"""
Convert DCM to MRP via quaternion
"""
DCM2MRPv2(C) = quat2MRP(DCM2quat(C))

"""
Convert quaternion to MRP
"""
function quat2MRP(β)
  ensure_quaternion(β)
  den = 1 + β[1]
  β[2:4]/den
end

"""
Successive rotation of MRPs a followed by b
"""
function compose_mrp(a, b)
  ensure_length(a, 3)
  a′ = reshape(a, (3,))
  ensure_length(b, 3)
  b′ = reshape(b, (3,))
  # @show a, b, 2 * dot(a,b)
  na2 = norm(a′)^2
  nb2 = norm(b′)^2
  den = 1 + na2*nb2 - 2 * dot(a′, b′)
  # @show den
  bb = abs(den) < 0.01 ? -b′ : b′
  den2 = 1 + na2*nb2 - 2 * dot(a′, bb)
  # @show bb, a′, typeof(bb), typeof(a′)
  num = (1 - na2)*bb + (1 - nb2)*a′ - 2*LinearAlgebra.cross(bb, a′)
  reshape(num/den2, (3,))
end

"""
'B' matrix relating MRPs σ to angular rates ω
"""
MRP_Bmatrix(σ) = I*(1 - normsq(σ)) + 2*tilde(σ) + 2σ*σ'

"""
Derivative of B matrix
"""
MRP_Bmatrix_derivative(σ, σ̇) = [
  2σ'σ̇ + 4σ[1]σ̇[1]                2(σ̇[1]σ[2] + σ[1]σ̇[2] - σ̇[3])   2(σ̇[1]σ[3] + σ[1]σ̇[3] + σ̇[2]);
  2(σ̇[1]σ[2] + σ[1]σ̇[2] + σ̇[3])   2σ'σ̇ + 4σ[2]σ̇[2]                2(σ̇[2]σ[3] + σ[2]σ̇[3] - σ̇[1]);
  2(σ̇[1]σ[3] + σ[1]σ̇[3] - σ̇[2])   2(σ̇[2]σ[3] + σ[2]σ̇[3] + σ̇[1])   2σ'σ̇ + 4σ[3]σ̇[3]
]

"""
Time derivative of MRP σ for angular velocity ω
"""
function dσdt_mrp(σ, ωBN)
  # @info "in dσdt_mrp"
  1/4 * MRP_Bmatrix(σ) * ωBN
end

"""
Angular velocity ω as function of MRP rates 
"""
  MRP_ω_from_σ_and_σ̇(σ, σ̇) = 4/(1 + normsq(σ))^2 * MRP_Bmatrix(σ)' * σ̇

"""
Angular acceleration ω̇
"""
function MRP_ω̇_from_σ_and_derivs(σ, σ̇, σ̈)
  B = MRP_Bmatrix(σ)
  Ḃᵀ = MRP_Bmatrix_derivative(σ, σ̇)'
  σ² = normsq(σ)
  4 * ((1+σ²)*Ḃᵀ*σ̇ + B'*(σ²*σ̈ + σ̈ - 4σ*σ̇'σ̇))/(1 + σ²)^3
end



"""
Return shadow MRP set if norm is close to 1
"""
function mrp_shadow(σ)
  σ² = normsq(σ)
  if σ² > 1.0
    # @infiltrate
    return -σ/σ²
  else
    return σ
  end
end

end