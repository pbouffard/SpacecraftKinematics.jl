module SpacecraftKinematics

# using Makie

primary_resolution() = (1920, 1080)

include("Utils.jl")
include("Rotations.jl")
# include("common.jl")


end # module
