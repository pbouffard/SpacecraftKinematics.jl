module Utils
using LinearAlgebra

export ensure_length, ensure_size, ensure_norm, ensure_unitvector, ensure_orthonormal

function ensure_length(x, ns; name="input")
  lx = length(x)
  # @show typeof(ns), (typeof(ns) <: Tuple)
  lengths_are_tuple = (typeof(ns) <: Tuple) && length(ns) != 1
  lx in ns || error("$(name) must have length $(lengths_are_tuple ? "∈" : "==") $ns, not $(lx)")
end

function ensure_size(x, sizes; name="input")
  if typeof(sizes) <: Tuple{T,T} where T
      # special case to allow easier entry of single size
      sizes2 = [sizes]
      errorstr = "size of $sizes"
  else
      sizes2 = sizes
      errorstr = "size ∈ $sizes2"
  end
  sx = size(x)
  sx in sizes2 || error("$name must have $(errorstr), not $sx")
end

function ensure_norm(x, n=1.0; name="input")
  nx = norm(x)
  nx ≈ n || error("$name must have norm ≈ $n, not $nx")
end

ensure_unitvector(x; name="input") = (length(x) > 1 || error("$name must have length > 1")) && ensure_norm(x, 1; name=name)

ensure_orthonormal(C; name="input") = C*C' ≈ I || error("$name must be orthonormal")

end