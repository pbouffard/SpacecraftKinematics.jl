module Common

# using Makie
using AbstractPlotting
using MakieUtils

using ..Rotations

export ASSETS_FOLDER
export setup_scene
export curscene
export triad!
export q321, q123

ASSETS_FOLDER = abspath(@__DIR__() * "/../assets/")
DEFAULT_RESOLUTION = (1500, 1500)

q321(a3, a2, a1) = AbstractPlotting.Quaternion(quat_wxyz2xyzw(DCM2quat(C321d(a3, a2, a1)))...)
q123(a3, a2, a1) = AbstractPlotting.Quaternion(quat_wxyz2xyzw(DCM2quat(C123d(a1, a2, a3)))...)

mutable struct SceneInfo
  parentscene::Scene
  scene::Scene
  hbox::Scene
  text::Scene
end


function setup_scene()
  
  # s = Scene(resolution=DEFAULT_RESOLUTION)
  # s = Scene(center = false, show_axis = false)
  # s2 = Scene(s)
  # t = text!(s2, "adding text",textsize = 0.6, position = (5.0, 1.1))
  # cam2d!(s2)
  # p = plot!(s, [Point3f0(0),Point3f0(0),Point3f0(0,0,0.01)])
  # another_triad = triad!(s, 25; translation=(0, 0, 25))
  # cam3d!(s)
  # display(s)
  # axis = s[Axis]
  # axis[:names][:axisnames] = ("x", "y", "z")
  # @show axis[:showaxis]
  
  # scene=content=scatter(rand(300,3))
  ps = Scene(; resolution=(1500,1000))
  s = Scene(; resolution=DEFAULT_RESOLUTION)
  origin_triad = triad!(s, 120; show_axis=true)
  cameracontrols(s).rotationspeed[] = 0.01f0
  textpos=lift(a->Vec2f0(widths(a)[1] ./ 2 , 0), pixelarea(ps))
  txt=text("my text", textsize=20, raw=true, position=textpos, camera=campixel!, align=(:center, :bottom), show_axis=false)
  hbox_ = hbox(s, txt, parent=ps)
  SceneInfo(ps, s, hbox_, txt)
end



curscene() = AbstractPlotting.current_scene()

end