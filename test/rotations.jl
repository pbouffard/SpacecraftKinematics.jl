using SpacecraftKinematics.Rotations
using LinearAlgebra

using SpacecraftKinematics.Rotations: ensure_rotation_matrix, ensure_quaternion
@testset "ensure_rotation_matrix" begin
  @test ensure_rotation_matrix(I(3))
  @test_throws ErrorException ensure_rotation_matrix(2*I(3))
  @test_throws ErrorException ensure_rotation_matrix(I(4))
  # TODO: more tests
end

@testset "ensure_quaternion" begin
  @test_throws ErrorException ensure_quaternion([1 0 0])
  @test_throws ErrorException ensure_quaternion([1.5 0 0 0])
  @test ensure_quaternion([1 0 0 0])
  @test ensure_quaternion([0 0 1+1e-10 0])
  # TODO more tests
end

@testset "tilde" begin
  @test_throws ErrorException tilde([2 1])
  @test tilde([0 0 0]) == zeros((3,3))
  @test tilde([1 0 0]) == [0 0 0; 0 0 -1; 0 1 0]
  @test tilde([1.0 2 3]) == [0 -3  2; 3 0 -1; -2  1  0]
end

@testset "normsq" begin
  @test normsq(1.0) == 1
  @test normsq(-2.0) == 4
  @test normsq([3 4]) == 25
end

# TODO: tests for C1, C1d, C2, C2d, C3, C3d

@testset "DCM2quat" begin
  @test DCM2quat(I(3)) == [1, 0, 0, 0]
  @test_throws ErrorException DCM2quat(zeros((3,3)))
  @test_throws ErrorException DCM2quat(zeros((4,4)))
  @test_throws ErrorException DCM2quat(zeros((4,4)))
  # TODO more tests
end

@testset "DCM ↔ MRP" begin
  @test_throws ErrorException MRP2DCM(zeros(4))
  @test MRP2DCM(zeros(3)) == I(3)
  @test DCM2MRP(I(3)) == zeros(3)
  @test MRP2DCM(DCM2MRP(I(3))) == I(3)
  @test MRP2DCM(DCM2MRP(C1d(90))) ≈ C1d(90)
  @test MRP2DCM(DCM2MRP(C2d(-45))) ≈ C2d(-45)
  @test MRP2DCM(DCM2MRPv2(C3d(180))) == C3d(180)
  @test MRP2DCM(DCM2MRP(C3d(180))) == C3d(180)
  @test MRP2DCM(DCM2MRP(C1d(180))) == C1d(180)
end

@testset "MRP composition" begin
  @test_throws ErrorException compose_mrp(zeros(4), zeros(3))
  @test compose_mrp(zeros(3), zeros(3)) == zeros(3)
  @test compose_mrp([1, 0, 0], zeros(3)) == [1.0; 0.0; 0.0]
end