using SpacecraftKinematics.Utils
using LinearAlgebra

@testset begin
  @test_throws ErrorException ensure_length([1 2], 1)
  @test_throws ErrorException ensure_length([1 2 3], (1, 2))
  @test ensure_length(1,1)
  @test ensure_length([1;2], 2)
end

@testset begin
  @test_throws ErrorException ensure_size([1 2], (2,1))
  @test_throws ErrorException ensure_size([1 2], ((2,1), (3, 1)))
  @test ensure_size(I(3), (3,3))
end

@testset begin
  @test_throws ErrorException ensure_norm(0.5)
  @test_throws ErrorException ensure_norm([0.5 0], 0.6)
  @test ensure_norm([1 0])
  @test ensure_norm([1 0], 1.0)
  @test ensure_norm([-2 0], 2.0)
end

@testset begin
  @test_throws ErrorException ensure_unitvector(1.5)  
  @test_throws ErrorException ensure_unitvector(1)
  @test_throws ErrorException ensure_unitvector([1 1])
  @test ensure_unitvector([1 0])
  @test ensure_unitvector([1.0 0.000001])
  @test_throws ErrorException ensure_unitvector([1.0 0.1])
end

@testset begin
  @test_throws ErrorException ensure_orthonormal(2*I(3))
  @test ensure_orthonormal(I(3))
  @test ensure_orthonormal(I(5))
  @test_throws ErrorException ensure_orthonormal(I(5) * (1 + 1e-7))
  @test ensure_orthonormal(I(5) * (1 + 1e-15))
end
